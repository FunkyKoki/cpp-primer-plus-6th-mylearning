#include <iostream>
#include "pe10-6_move.h"

Move::Move(double a, double b){
    x = a;
    y = b;
}

void Move::showmove() const{
    std::cout << "x: " << x << " y: " << y << std::endl;
}

Move Move::add(const Move & m) const{
    double x_ = m.x + this->x;
    double y_ = m.y + this->y;
    Move temp(x_, y_);
    return temp;
}

void Move::reset(double a, double b){
    x = a;
    y = b;
}