#ifndef PLORG_H_
#define PLORG_H_

class Plorg{
private:
    char name[20];
    int CI;
public:
    Plorg(char * n = "Plorga", int ci = 50);
    bool setCI(int ci);
    void showPlorg() const;
};

#endif