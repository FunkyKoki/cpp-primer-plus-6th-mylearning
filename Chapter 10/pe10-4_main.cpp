#include <iostream>
#include "pe10-4_sales.h"

int main(int argc, char const *argv[])
{
    {
        const double ar[3] = {12.0, 5.6, 8.96};
        using namespace SALES;
        Sales s1;
        Sales s2(ar, 3);
        s1.showSales();
        s2.showSales();
    }
    return 0;
}
