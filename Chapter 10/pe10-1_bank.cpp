#include <iostream>
#include "pe10-1_bank.h"

Bank::Bank(){
    name = "null";
    acount = 0;
    money = 0;
}

Bank::Bank(std::string inputname, long aco, long mon){
    name = inputname;
    acount = aco;
    money = mon;
}

Bank::~Bank(){
    std::cout << "Bye!" << std::endl; 
}

void Bank::show() const{
    std::cout << "Bank Name: " << name << std::endl;
    std::cout << "Bank Acount: " << acount << std::endl;
    std::cout << "Bank Money: " << money << std::endl;
}

bool Bank::pushMoney(long pushin){
    if(pushin < 0){
        std::cout << "Error: Input number less than 0!\n";
        return false;
    }
    else{
        money += pushin;
    }
}

bool Bank::popMoney(long popout){
    if(popout < 0){
        std::cout << "Error: Input number less than 0!\n";
        return false;
    }
    else if(popout > money){
        std::cout << "Error: You do not have that much money!\n";
        return false;
    }
    else{
        money -= popout;
        return true;
    }
}