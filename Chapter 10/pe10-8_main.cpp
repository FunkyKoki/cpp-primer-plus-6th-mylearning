#include <iostream>
#include "pe10-8_list.h"

void show(Item *);

int main(int argc, char const *argv[])
{
    {
        List l;
        Item a = 2500;
        l.add(a);
        std::cout << "Isempty? " << l.isempty()
                  << "  Isfull? " << l.isfull() <<std::endl;
        l.visit((*show));
    }
    return 0;
}

void show(Item * item){
    for(int i = 0; i < MAX; i++){
        std::cout << item[i] << " ";
    }
    std::cout << std::endl;
}