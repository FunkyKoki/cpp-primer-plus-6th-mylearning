#ifndef BANK_H_
#define BANK_H_

#include <string>

class Bank{
private:
    std::string name;
    long acount;
    long money;
public:
    Bank();
    Bank(std::string inputname, long aco = 0, long mon = 0);
    ~Bank();
    void show() const;
    bool pushMoney(long pushin);
    bool popMoney(long popout);
};

#endif