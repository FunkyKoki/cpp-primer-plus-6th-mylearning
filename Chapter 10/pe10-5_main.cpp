// stacker.cpp -- testing the Stack class
#include <iostream>
#include "pe10-5_customer.h"
int main()
{
    {
        Item a = {"hello mun", 100};
        Item b;
        Stack st;
        st.push(a);
        st.pop(b);
    }
    std::cout << "Bye\n";
    return 0; 
}
