#include <iostream>
#include "pe10-8_list.h"

List::List(){
    top = 0;
    for(int i = 0; i < MAX; i++){
        item[i] = 0;
    }
}

bool List::add(Item & i){
    if(top >= MAX){
        std::cout << "Error: The list is full!\n";
        return false;
    }
    else{
        item[top++] = i;
        return true;
    }
}

bool List::isfull() const{
    return top >= MAX;
}

bool List::isempty() const{
    return top == 0;
}

void List::visit(void (*pf)(Item *)){
    pf(item);
}