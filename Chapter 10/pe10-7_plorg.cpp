#include <iostream>
#include <cstring>
#include "pe10-7_plorg.h"

Plorg::Plorg(char * n, int ci){
    if(strlen(n) > 19){
        std::cout << "Error: Too many charactors!\n";
    }
    else{
        strcpy(name, n);
        CI = ci;
    }
}

bool Plorg::setCI(int ci){
    CI = ci;
}

void Plorg::showPlorg() const{
    std::cout << "Name: " << name << " CI: " << CI << std::endl;
}