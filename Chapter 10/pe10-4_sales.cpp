#include <iostream>
#include "pe10-4_sales.h"

namespace SALES{
    Sales::Sales(){
        double sum = 0;
        for(int i = 0; i < QUARTERS; i++){
            std::cout << "Input sales[" << i <<"]: ";
            std::cin >> sales[i];
            sum += sales[i];
        }
        /* Another input method */
        // Anthor: JSP
        // input number in this way: "12.0 5.6 8.96 10.8"
        /* for(int i = 0; i < QUARTERS; i++){
            std::cout << "Input sales: ";
            std::cin >> s.sales[i];
            sum += s.sales[i];
        } */
        average = sum / QUARTERS;
        max = sales[0], min = sales[0];
        for(int i = 1; i < QUARTERS; i++){
            if(sales[i] > max){
                max = sales[i];
            }
            if(sales[i] < min){
                min = sales[i];
            }
        }
    }
    Sales::Sales(const double ar[], int n){
        double sum = 0;
        for(int i = 0; i < n; i++){
            sales[i] = ar[i];
            sum += sales[i];
        }
        average = sum / n;
        max = sales[0], min = sales[0];
        for(int i = 1; i < n; i++){
            if(sales[i] > max){
                max = sales[i];
            }
            if(sales[i] < min){
                min = sales[i];
            }
        }
    }
    void Sales::showSales() const{
        std::cout << "############\n"
        "max: " << max << "\n"
        "min: " << min << "\n"
        "average: " << average << "\n";
    }
}