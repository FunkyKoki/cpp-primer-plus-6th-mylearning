#include <iostream>
#include <cstring>
#include "pe10-3_golf.h"

Golf::Golf(){
    char next;
    std::cout << "Input name: ";
    std::cin.get(fullname, Len);
    if(std::cin){
        std::cin.get(next);
        while(next != '\n'){
            std::cin.get(next);
        }
        std::cout << "Input handicap: ";
        std::cin >> handicap;
        std::cin.get(next);
        while(next != '\n'){
            std::cin.get(next);
        }
    }
    else{
	    std::cout << "NO INPUT!" << std::endl;
    }
}

Golf::Golf(const char * name, int hc){
    strcpy(fullname, name);
    handicap = hc;
}

void Golf::setHandicap(int hc){
    handicap = hc;
}

void Golf::showgolf() const{
    std::cout << "Name: " << fullname << std::endl;
    std::cout << "Handicap: " << handicap << std::endl;
}