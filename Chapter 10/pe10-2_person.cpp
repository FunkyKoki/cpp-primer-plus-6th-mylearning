#include <iostream>
#include <cstring>
#include "pe10-2_person.h"

Person::Person(const std::string & ln, const char * fn){
    lname = ln;
    strcpy(fname, fn);
}

void Person::Show() const{
    std::cout << "Firstname: " << fname
              << "  Lastname: " << lname << std::endl;
}

void Person::FormalShow() const{
    std::cout << "Lastname: " << lname 
              << "  Firstname: " << fname << std::endl;
}
