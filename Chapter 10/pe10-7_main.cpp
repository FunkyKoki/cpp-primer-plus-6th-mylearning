#include <iostream>
#include "pe10-7_plorg.h"

int main(int argc, char const *argv[])
{
    {
        Plorg a("balabala", 12);
        a.showPlorg();
        a.setCI(15000);
        a.showPlorg();
    }
    return 0;
}
