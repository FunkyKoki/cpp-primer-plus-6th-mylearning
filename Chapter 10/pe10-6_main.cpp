#include <iostream>
#include "pe10-6_move.h"

int main(int argc, char const *argv[])
{
    {
        Move p(10, 30);
        Move q;
        p.showmove();
        q.showmove();
        q.reset(30, 10);
        Move temp = p.add(q);
        temp.showmove();
    }
    return 0;
}
