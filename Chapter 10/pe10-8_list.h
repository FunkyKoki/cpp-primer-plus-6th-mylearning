#ifndef LIST_H_
#define LIST_H_

typedef unsigned int Item; //change type
static const int MAX = 5;

class List{
private:
    Item item[MAX];
    int top = 0;
public:
    List();
    bool add(Item &);
    bool isfull() const;
    bool isempty() const;
    void visit(void (*pf)(Item *));
};

#endif