#include <iostream>
#include "pe10-3_golf.h"

const int MAX_COUNT = 3;

int main(){
    Golf info[MAX_COUNT] = {
        Golf("jin", 12),
        Golf("qwerty", 23)
    };
    int count = 0;
    while(count < MAX_COUNT){
        info[count].showgolf();
	    count++;
    }
    info[0].setHandicap(10000);
    info[0].showgolf();
}