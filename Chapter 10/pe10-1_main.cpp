#include <iostream>
#include "pe10-1_bank.h"

int main(int argc, char const *argv[]){
    {
        Bank jin("jin", 201514144, 300000);
        jin.show();
        jin.pushMoney(-100);
        jin.show();
        jin.popMoney(2000000);
        jin.show();
    }
    return 0;
}
