#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using std::cin;
using std::cout;
using std::endl;
using std::list;
using std::vector;
using std::sort;
using std::srand;
using std::time;

int main(){
	srand(time(0));
	const unsigned long NUM = 100000;
	vector<int> vi0(NUM);
	for (int i = 0; i < NUM; i++){
		vi0[i] = rand() % 10000;
	}
	vector<int> vi(vi0);
	list<int> li;
	li.insert(li.begin(), vi0.begin(), vi0.end());

	clock_t start = clock();
	sort(vi.begin(), vi.end());
	clock_t end = clock();
	cout << "Vector sort time: " << (double)(end - start) / CLOCKS_PER_SEC << endl;

	start = clock();
	li.sort();
	end = clock();
	cout << "List sort time: " << (double)(end - start) / CLOCKS_PER_SEC << endl;

	li.insert(li.begin(), vi0.begin(), vi0.end());

	start = clock();
	vi.insert(vi.begin(), li.begin(), li.end());
	sort(vi.begin(), vi.end());
	li.insert(li.begin(), vi.begin(), vi.end());
	end = clock();
	cout << "Vector sort time: " << (double)(end - start) / CLOCKS_PER_SEC << endl;

	return 0;
}