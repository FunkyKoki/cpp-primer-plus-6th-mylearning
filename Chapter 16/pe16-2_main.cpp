#include <iostream>
#include <string>
#include <iterator>
#include <algorithm>
using std::cin;
using std::cout;
using std::endl;
using std::string;

bool judgeHuiwen(string & str);

int main(){
	string s;
	cout << "Please input the string(quit to quit): ";
	std::getline(cin, s);

	while (s != "quit"){
		cout << (judgeHuiwen(s) == true ? "True" : "False") << endl;
		cout << "Please input the string(quit to quit): ";
		std::getline(cin, s);
	}
}

bool judgeHuiwen(string & str){
	unsigned int alphaCount = 0;
	for (unsigned int i = 0; i < str.length(); i++){
		if ((str[i] > 'A' && str[i] < 'Z') || (str[i] >'a' && str[i] < 'z')){
			alphaCount++;
		}
	}
	string temp(alphaCount, ' ');
	for (unsigned int i = 0; i < str.length(); i++){
		if ((str[i] > 'A' && str[i] < 'Z') || (str[i] >'a' && str[i] < 'z')){
			temp[--alphaCount] = str[i];
		}
	}
	std::transform(temp.begin(), temp.end(), temp.begin(), tolower);
	if (temp.length() == 1){
		return true;
	}
	for (unsigned int i = 0; i < temp.length() / 2; i++){
		if (temp[i] != temp[temp.length() - i - 1]){
			return false;
		}
	}
	return true;
}