#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::endl;

void Show(string & s){ cout << s << "; "; }

int main(){
	vector<string> Mat;
	vector<string> Pat;
	vector<string> Sum;

	string temp;
	cout << "Now, ##Mat## input the name(quit to quit): ";
	std::getline(cin, temp);
	while (temp != "quit"){
		Mat.push_back(temp);
		cout << "Now, ##Mat## input the name(quit to quit): ";
		std::getline(cin, temp);
	}
	std::sort(Mat.begin(), Mat.end());
	cout << "##Mat## friends: \n";
	std::for_each(Mat.begin(), Mat.end(), Show);
	cout << endl;
	cout << "Now, **Pat** input the name(quit to quit): ";
	std::getline(cin, temp);
	while (temp != "quit"){
		Pat.push_back(temp);
		cout << "Now, **Pat** input the name(quit to quit): ";
		std::getline(cin, temp);
	}
	std::sort(Pat.begin(), Pat.end());
	cout << "**Pat** friends: \n";
	std::for_each(Pat.begin(), Pat.end(), Show);
	cout << endl;

	Sum.insert(Sum.begin(), Mat.begin(), Mat.end());
	Sum.insert(Sum.end(), Pat.begin(), Pat.end());
	cout << "Sum friends: \n";
	std::for_each(Sum.begin(), Sum.end(), Show);
	cout << endl;

	return 0;
}