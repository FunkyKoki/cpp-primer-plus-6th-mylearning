#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

bool judgeHuiwen(string & str);

int main(){
	string s;
	cout << "Please input the string(quit to quit): ";
	cin >> s;
	while (cin.get() != '\n'){
		continue;
	}
	while (s != "quit"){
		cout << (judgeHuiwen(s) == true ? "True" : "False") << endl;
		cout << "Please input the string(quit to quit): ";
		cin >> s;
		while (cin.get() != '\n'){
			continue;
		}
	}
}

bool judgeHuiwen(string & str){
	if (str.length() == 1){
		return true;
	}
	for (unsigned int i = 0; i < str.length() / 2; i++){
		if (str[i] != str[str.length() - i - 1]){
			return false;
		}
	}
	return true;
}