#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

vector<int> Lotto(int range, int n);
void Show(int n) { cout << n << " "; }

int main(){
	std::srand(std::time(0));
	vector<int> winners;
	winners = Lotto(51, 6);
	std::for_each(winners.begin(), winners.end(), Show);
	cout << endl;
	return 0;
}

vector<int> Lotto(int range, int n){
	vector<int> v(range);
	for (int i = 1; i <= range; i++){
		v[i - 1] = i;
	}
	std::random_shuffle(v.begin(), v.end());
	v.erase(v.begin() + n, v.end());
	return v;
}