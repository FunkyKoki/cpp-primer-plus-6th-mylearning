#include <iostream>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

int reduce(long ar[], int n);

int main(){
	const int NUM = 10;
	long old[NUM] = { 2, 2, -8, 9, 387, 2, 45, -8, 0, 0 };
	cout << "After sort: " << reduce(old, NUM) << endl;
	return 0;
}

int reduce(long ar[], int n){
	vector<long> temp(ar, ar + n);
	std::sort(temp.begin(), temp.end());
	auto p = std::unique(temp.begin(), temp.end());
	temp.erase(p, temp.end());
	return temp.size();
}