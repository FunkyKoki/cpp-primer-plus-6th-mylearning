#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>

using namespace std;

struct Review {
	std::string title;
	int rating;
	double price;
};

bool operator<(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2);
bool worseThan(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2);
bool cheaperThan(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2);
shared_ptr<Review> FillReview();
void ShowReview(const shared_ptr<Review> & rr);

int main()
{
	shared_ptr<Review> p(new Review);

	vector<shared_ptr<Review> > books;

	shared_ptr<Review> temp;

	while (temp = FillReview())
		books.push_back(temp);
	if (books.size() > 0)
	{
		string choice;
		cout << "Now please make your choice:\n"
			<< "\t\'origin\' for origional order\n"
			<< "\t\'alpha\' for alpha order\n"
			<< "\t\'rateup\' for up order of rating\n"
			<< "\t\'ratedown\' for down order of rating\n"
			<< "\t\'priceup\' for up order of price\n"
			<< "\t\'pricedown\' for down order of price\n"
			<< "\t\'quit\' for quit: ";
		cin >> choice;
		cin.get();
		while (choice != "quit"){
			if (choice == "origin"){
				for_each(books.begin(), books.end(), ShowReview);
			}
			else if (choice == "alpha"){
				sort(books.begin(), books.end());
				cout << "Sorted by title:\nRating\tBook\n";
				for_each(books.begin(), books.end(), ShowReview);
			}
			else if (choice == "rateup"){
				sort(books.begin(), books.end(), worseThan);
				cout << "Sorted by rating:\nRating\tBook\n";
				for_each(books.begin(), books.end(), ShowReview);
			}
			else if (choice == "ratedown"){
				sort(books.begin(), books.end(), worseThan);
				cout << "Sorted by rating:\nRating\tBook\n";
				for_each(books.rbegin(), books.rend(), ShowReview);
			}
			else if (choice == "priceup"){
				sort(books.begin(), books.end(), cheaperThan);
				cout << "Sorted by price:\nRating\tBook\n";
				for_each(books.begin(), books.end(), ShowReview);
			}
			else if (choice == "pricedown"){
				sort(books.begin(), books.end(), cheaperThan);
				cout << "Sorted by price:\nRating\tBook\n";
				for_each(books.rbegin(), books.rend(), ShowReview);
			}
			cout << "Now please make your choice:\n"
				<< "\t\'origin\' for origional order\n"
				<< "\t\'alpha\' for alpha order\n"
				<< "\t\'rateup\' for up order of rating\n"
				<< "\t\'ratedown\' for down order of rating\n"
				<< "\t\'priceup\' for up order of price\n"
				<< "\t\'pricedown\' for down order of price\n"
				<< "\t\'quit\' for quit: ";
			cin >> choice;
			cin.get();
		}
	}
	else
		cout << "No entries. ";
	cout << "Bye.\n";
	// cin.get();
	return 0;
}

bool operator<(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2)
{
	if (r1->title < r2->title)
		return true;
	else if (r1->title == r2->title && r1->rating < r2->rating)
		return true;
	else
		return false;
}

bool worseThan(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2)
{
	if (r1->rating < r2->rating)
		return true;
	else
		return false;
}

bool cheaperThan(const shared_ptr<Review> & r1, const shared_ptr<Review> & r2)
{
	if (r1->price < r2->price)
		return true;
	else
		return false;
}

shared_ptr<Review> FillReview()
{
	shared_ptr<Review> t(new Review);
	std::cout << "Enter book title (quit to quit): ";
	std::getline(std::cin, t->title);
	if (t->title == "quit")
		return 0;
	std::cout << "Enter book rating: ";
	std::cin >> t->rating;
	std::cout << "Enter book price: ";
	std::cin >> t->price;
	if (!std::cin)
		return 0;
	// get rid of rest of input line
	while (std::cin.get() != '\n')
		continue;
	return t;
}

void ShowReview(const shared_ptr<Review> & rr)
{
	std::cout << rr->rating << "\t" << rr->title << "\t" << rr->price << std::endl;
}
