#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

template <typename T>
int reduce(T ar[], int n);

int main(){
	const int NUM = 10;
	long old1[NUM] = { 2, 2, -8, 9, 387, 2, 45, -8, 0, 0 };
	string old2[NUM] = {"Jsp", "Jsp", "Jsp", "Jsp", "Jasdadsp", "sq", "cfa", "cvaqvc", "cqafw", "c vavf"};
	cout << "long After sort: " << reduce(old1, NUM) << endl;
	cout << "string After sort: " << reduce(old2, NUM) << endl;
	return 0;
}

template <typename T>
int reduce(T ar[], int n){
	vector<T> temp(ar, ar + n);
	std::sort(temp.begin(), temp.end());
	auto p = std::unique(temp.begin(), temp.end());
	temp.erase(p, temp.end());
	return temp.size();
}