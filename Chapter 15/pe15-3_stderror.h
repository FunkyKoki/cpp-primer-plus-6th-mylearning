// exc_mean.h  -- exception classes for hmean(), gmean()
#include <iostream>
#include <string>
#include <stdexcept>

class bad_hmean : public std::logic_error
{
private:
	double v1;
	double v2;
public:
	bad_hmean(double a, double b, 
		const std::string & s = "Index error in Bad_Hmean object\n")
		: v1(a), v2(b), std::logic_error(s) {}
	void tell() { std::cout << "Value: " << v1 << " " << v2 
		<< "\nBad Hmean: invalid arguments.\n"; }
	//~bad_hmean() throw() {}
};

class bad_gmean : public std::logic_error
{
private:
	double v1;
	double v2;
public:
	bad_gmean(double a, double b,
		const std::string & s = "Index error in Bad_Gmean object\n")
		: v1(a), v2(b), std::logic_error(s) {}
	void tell() { std::cout << "Value: " << v1 << " " << v2 
		<< "\nBad Gmean: arguments should be >= 0\n"; }
	//~bad_gmean() throw() {}
};
