// exc_mean.h  -- exception classes for hmean(), gmean()
#include <iostream>
#include <string>
#include <stdexcept>

class bad_hmean : public std::logic_error
{
public:
	bad_hmean(const std::string & s = "Index error in Bad_Hmean object\n")
		: std::logic_error(s) {}
	//const char * what() { return "Bad Hmean:  invalid arguments.\n"; }
	~bad_hmean() throw() {}
};

class bad_gmean : public std::logic_error
{
public:
	bad_gmean(const std::string & s = "Index error in Bad_Gmean object\n")
		: std::logic_error(s) {}
	//const char * what() { return "Bad Gmean: arguments should be >= 0\n"; }
	~bad_gmean() throw() {}
};
