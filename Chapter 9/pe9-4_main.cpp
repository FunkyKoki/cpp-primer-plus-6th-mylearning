#include <iostream>
#include "pe9-4_sales.h"

int main(){
    const double ar[4] = {12.0, 5.6, 8.96, 10.8};
    using namespace SALES;
    Sales s1, s2;
    setSales(s1, ar, 4);
    setSales(s2);
    showSales(s1);
    showSales(s2);
}