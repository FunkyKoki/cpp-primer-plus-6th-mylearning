#include <iostream>
#include "pe9-1_golf.h"

void setgolf(golf & g, const char * name, int hc){
    g.fullname[Len] = *name;
    g.handicap = hc;
}

int setgolf(golf & g){
    char next;
    std::cout << "Input name: ";
    std::cin.get(g.fullname, Len);
    if(std::cin){
        std::cin.get(next);
        while(next != '\n'){
            std::cin.get(next);
        }

        std::cout << "Input handicap: ";
        std::cin >> g.handicap;

        std::cin.get(next);
        while(next != '\n'){
            std::cin.get(next);
        }

	return 1;
    }
    else{
	std::cout << "NO INPUT!" << std::endl;
        return 0;
    }
}

void handicap(golf & g, int hc){
    g.handicap = hc;
}

void showgolf(const golf & g){
    std::cout << "Name: " << g.fullname << std::endl;
    std::cout << "Handicap: " << g.handicap << std::endl;
}
