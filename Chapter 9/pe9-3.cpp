#include <iostream>
#include <cstring>

struct chaff{
    char dross[20];
    int slag;
};

int main(){
    char stuff[50];
    chaff * p1 = new (stuff) chaff;
    chaff * p2 = new (stuff + sizeof(chaff)) chaff;
    /* Tips */
    // Author: JSP
    // 字符串结尾默认有字符'\0'，因此给dorss[20]赋值时,
    // 最多使用一个含有19个字符的字符串，其末尾自动添加'\0'
    strcpy(p1->dross, "abcdefghijklmnopqrs");
    p1->slag = 666;
    strcpy(p2->dross, "ABCDEFGHIJKLMNOPQRS");
    p2->slag = 888;
    std::cout << p1->dross << std::endl;
    std::cout << p1->slag << std::endl;
    std::cout << p2->dross << std::endl;
    std::cout << p2->slag << std::endl;
    return 0;
}
