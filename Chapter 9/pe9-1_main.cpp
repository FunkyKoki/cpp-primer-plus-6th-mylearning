#include <iostream>
#include "pe9-1_golf.h"

const int MAX_COUNT = 3;

int main(){
    golf info[MAX_COUNT];
    int count = 0;
    while(count < MAX_COUNT && setgolf(info[count])){
        showgolf(info[count]);
	count++;
    }
}
