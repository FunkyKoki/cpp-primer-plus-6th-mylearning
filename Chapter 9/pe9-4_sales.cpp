#include <iostream>
#include "pe9-4_sales.h"

namespace SALES{
    void setSales(Sales & s, const double ar[], int n){
        double sum = 0;
        for(int i = 0; i < n; i++){
            s.sales[i] = ar[i];
            sum += s.sales[i];
        }
        s.average = sum / n;
        s.max = s.sales[0], s.min = s.sales[0];
        for(int i = 1; i < n; i++){
            if(s.sales[i] > s.max){
                s.max = s.sales[i];
            }
            if(s.sales[i] < s.min){
                s.min = s.sales[i];
            }
        }
    }
    void setSales(Sales & s){
        double sum = 0;
        for(int i = 0; i < QUARTERS; i++){
            std::cout << "Input sales[" << i <<"]: ";
            std::cin >> s.sales[i];
            sum += s.sales[i];
        }
        /* Another input method */
        // Anthor: JSP
        // input number in this way: "12.0 5.6 8.96 10.8"
        /* for(int i = 0; i < QUARTERS; i++){
            std::cout << "Input sales: ";
            std::cin >> s.sales[i];
            sum += s.sales[i];
        } */
        s.average = sum / QUARTERS;
        s.max = s.sales[0], s.min = s.sales[0];
        for(int i = 1; i < QUARTERS; i++){
            if(s.sales[i] > s.max){
                s.max = s.sales[i];
            }
            if(s.sales[i] < s.min){
                s.min = s.sales[i];
            }
        }
    }
    void showSales(const Sales & s){
        std::cout << "############\n"
        "max: " << s.max << "\n"
        "min: " << s.min << "\n"
        "average: " << s.average << "\n";
    }
}