#include <iostream>

template <typename T>
T sumArray(T arr[], int n);

template <typename T>
T sumArray(T * arr[], int n);

struct debts
{
    char name[50];
    double amount;
};

int main()
{
    using namespace std;
    int things[6] = {13, 31, 103, 301, 310, 130};
    struct debts mr_E[3] =
    {
        {"Ima Wolfe", 2400.0},
        {"Ura Foxe", 1300.0},
        {"Iby Stout", 1800.0}
    };
    double * pd[3]; 

    for (int i = 0; i < 3; i++)
        pd[i] = &mr_E[i].amount;
    
    cout << "Sum Mr. E's counts of things:\n";
    cout << sumArray(things, 6) << endl;
    cout << "Sum Mr. E's debts:\n";
    cout << sumArray(pd, 3) << endl;
    return 0;
}

template <typename T>
T sumArray(T arr[], int n){
    T sum = arr[0];
    for(int i = 0; i < n - 1; i++){
        sum += arr[i+1];
    }
    return sum;
}

template <typename T>
T sumArray(T * arr[], int n){
    T sum = *arr[0];
    for(int i = 0; i < n - 1; i++){
        sum += *arr[i+1];
    }
    return sum;
}
