#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

void change(string & str);

int main(int argc, char** argv) {
	string str;
	cout << "Enter a string(q to quit): "; 
	cin >> str;
	while(str != "q"){
		change(str);
		cout << "Enter a string(q to quit): ";
		cin >> str;
	}
	cout << "Bye." << endl;
	return 0;
}

void change(string & str) {
	transform(str.begin(), str.end(), str.begin(), ::toupper);
	cout << str << endl;
}
