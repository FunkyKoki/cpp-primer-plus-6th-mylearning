#include <iostream>
#include <cstring>
using namespace std;

template <typename T>
T maxn(T a[], const int n);

template <> char * maxn<char *>(char * a[], const int n);

int main(){
	double a[4] = { 6.2, 3.2, -1.0, 2.0 };
	int b[6] = { 20, 5, 10, 1, 20, -5 };
	char * str[5] = { "a", "bb", "ccc", "dddd", "eeeee" };
	cout << "int result: " << endl;
	cout << maxn(b, 6) << endl;
	cout << "double result: " << endl;
	cout << maxn(a, 4) << endl;
	cout << "char* result: " << endl;
	cout << maxn(str, 5) << endl;
	return 0;
}

template <typename T>
T maxn(T a[], const int n){
	decltype(a[0]) max = a[0];
	for (int i = 0; i < (n - 1); i++){
		if (a[i + 1] > max){
			max = a[i + 1];
		}
	}
	return max;
}

template <> char * maxn<char *>(char * a[], const int n){
	char * & longest = a[0];
	for (int i = 0; i < (n - 1); i++){
		if (strlen(a[i + 1]) > strlen(longest)){
			longest = a[i + 1];
		}
	}
	return longest;
}
