#include <iostream>
using namespace std;

template <typename T>
T max5(const T a[]);

int main(){
	const double a[5] = {1.0, 6.2, 3.2, -1.0, 2.0};
	const int b[5] = {5, 10, 1, 20, -5};
	cout << "double result: " << endl; 
	cout << max5(a) << endl;
	cout << "int result: " << endl; 
	cout << max5(b) << endl;
	return 0;
}

template <typename T>
T max5(const T a[]){
	T max = a[0];
	for(int i = 0; i < 4; i++){
		if(a[i+1] > max){
			max = a[i+1];
		}
	}
	return max;
}
