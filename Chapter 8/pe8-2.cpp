#include <iostream>
using namespace std;
struct CandyBar{
    char *label;
    float weight;
    int heat;
};
void set(CandyBar & bar, char str[] = "Millennium Munch", float w = 2.85, int h = 350);
void show(const CandyBar & bar);

int main(){
    CandyBar bar;
    set(bar, "hdciubisbcisbichisu");
    show(bar);
    return 0;
}

void set(CandyBar & bar, char str[], float w, int h){
    bar.label = str;
    bar.weight = w;
    bar.heat = h;
}
void show(const CandyBar & bar){
    cout << bar.label <<endl;
    cout << bar.weight <<endl;
    cout << bar.heat <<endl;
}
