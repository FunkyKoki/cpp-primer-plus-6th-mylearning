#include "pe13-3_dma.h"
#include <cstring>

// DMA methods
DMA::DMA(const std::string & l){
    label = l;
}

DMA::DMA(const DMA & dma){
    label = dma.label;
}

DMA::~DMA(){
    
}

DMA & DMA::operator=(const DMA & dma){
    if(this == &dma){
        return *this;
    }
    else{
        label = dma.label;
        return *this;
    }
}

void DMA::View() const{
    std::cout << "DMA: label: " << label << std::endl;
}

std::ostream & operator<<(std::ostream & os, const DMA & dma){
    os << "DMA: label: " << dma.label << std::endl;
    return os;
}

// baseDMA methods
baseDMA::baseDMA(const std::string & l, int r) : DMA(l)
{
    rating = r;
}

baseDMA::baseDMA(const DMA & dma, int r) : DMA(dma)
{
    rating = r;
}

void baseDMA::View() const{
    DMA::View();
    std::cout << "baseDMA: rating: " << rating << std::endl;
}

std::ostream & operator<<(std::ostream & os, const baseDMA & rs)
{
    os << (const DMA &)rs;
    os << "baseDMA: Rating: " << rs.rating << std::endl;
    return os;
}

// lacksDMA methods
lacksDMA::lacksDMA(const std::string & l, const std::string & c , int r)
                  : DMA(l)
{
    color = c;
    rating = r;
}

lacksDMA::lacksDMA(const DMA & dma, const std::string & c, int r)
                  : DMA(dma)
{
    color = c;
    rating = r;
}

void lacksDMA::View() const{
    DMA::View();
    std::cout << "lacksDMA: color: " << color << std::endl;
    std::cout << "         rating: " << rating << std::endl;
}

std::ostream & operator<<(std::ostream & os, const lacksDMA & ls)
{
    os << (const DMA &) ls;
    os << "lacksDMA: Color: " << ls.color << std::endl;
    os << "         rating: " << ls.color << std::endl;
    return os;
}

// hasDMA methods
hasDMA::hasDMA(const std::string & l, const std::string & s, int r)
              : DMA(l)
{
    style = s;
    rating = r;
}

hasDMA::hasDMA(const DMA & dma, const std::string & s, int r)
         : DMA(dma)
{
    style = s;
    rating = r;
}

hasDMA::hasDMA(const hasDMA & hs)
         : DMA(hs)  // invoke base class copy constructor
{
    style = hs.style;
    rating = hs.rating;
}

hasDMA::~hasDMA()
{
    
}

void hasDMA::View() const{
    DMA::View();
    std::cout << "hasDMA: style: " << style << std::endl;
    std::cout << "       rating: " << rating << std::endl;
}

hasDMA & hasDMA::operator=(const hasDMA & hs)
{
    if (this == &hs)
        return *this;
    DMA::operator=(hs);  // copy base portion
    style = hs.style;
    rating = hs.rating;
    return *this;
}
    
std::ostream & operator<<(std::ostream & os, const hasDMA & hs)
{
    os << (const DMA &) hs;
    os << "hasDMA: style: " << hs.style << std::endl;
    os << "       rating: " << hs.rating << std::endl;
    return os;
}
