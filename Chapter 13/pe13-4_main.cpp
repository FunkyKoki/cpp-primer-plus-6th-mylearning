#include "pe13-4_port.h"

int main(int argc, char const *argv[])
{
    {
        Port p("Gallo", "tawny", 20);
        Port p1 = p;
        Port p2;
        p2 = p;
        p1 += 10;
        p2 -= 10;
        cout << p2.BottleCount() << endl;
        p.Show();
        p1.Show();
        p2.Show();
        cout << p << p1 << p2 << endl;
        VintagePort v("Jsp", 50, "AJin", 1996);
        v += 100;
        v -= 20;
        cout << v.BottleCount() << endl;
        v.Show();
    }
    return 0;
}
