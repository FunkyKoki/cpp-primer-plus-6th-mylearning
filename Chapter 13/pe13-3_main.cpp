#include <iostream>
#include "pe13-3_dma.h"

const int DMAS = 3;

int main()
{
   using std::cin;
   using std::cout;
   using std::endl;

   DMA * p_dmas[DMAS];
   std::string temp;
   char kind;

   for (int i = 0; i < DMAS; i++)
   {
        cout << "Enter dma's label: ";
		getline(cin,temp);
        cout << "Enter 1 for baseDMA or "
             << "2 for lacksDMA or "
             << "3 for hasDMA: ";
        while (cin >> kind && (kind != '1' && kind != '2' && kind != '3'))
            cout <<"Enter either 1 or 2 or 3: ";
        while (cin.get() != '\n')
            continue;
        if (kind == '1'){   // Jsp: can also use switch...case...
            int temprating;
            cout << "Enter the rating: ";
            cin >> temprating;
            p_dmas[i] = new baseDMA(temp, temprating);
        }
        else if(kind == '2'){
            int temprating;
            std::string tempcolor;
            cout << "Enter the color: ";
            getline(cin,tempcolor);
            cout << "Enter the rating: ";
            cin >> temprating;
            p_dmas[i] = new lacksDMA(temp, tempcolor, temprating);\
        }
        else{
            int temprating;
            std::string tempstyle;
            cout << "Enter the style: ";
            getline(cin,tempstyle);
            cout << "Enter the rating: ";
            cin >> temprating;
            p_dmas[i] = new hasDMA(temp, tempstyle, temprating);
         }
         while (cin.get() != '\n')
             continue;
   }
   cout << endl;
   for (int i = 0; i < DMAS; i++)
   {
       p_dmas[i]->View();
       cout << endl;
   }
              
   for (int i = 0; i < DMAS; i++)
   {
       delete p_dmas[i];  // free memory
   }
   cout << "Done.\n";
   return 0; 
}
