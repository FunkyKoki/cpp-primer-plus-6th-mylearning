#ifndef DMA_H_
#define DMA_H_

#include <iostream>

class DMA{
private:
    std::string label;
public:
    DMA(const std::string & l = "null");
    DMA(const DMA & dma);
    virtual ~DMA();
    DMA & operator=(const DMA & dma);
    virtual void View() const;
    friend std::ostream & operator<<(std::ostream & os, const DMA & dma);
};

//  Base Class Using DMA
class baseDMA :public DMA
{
private:
    int rating;
public:
    baseDMA(const std::string & l = "null", int r = 0);
    baseDMA(const DMA & dma, int r = 0);
    void View() const;
    friend std::ostream & operator<<(std::ostream & os, 
                                     const baseDMA & rs);
};

// derived class without DMA
// no destructor needed
// uses implicit copy constructor
// uses implicit assignment operator
class lacksDMA :public DMA
{
private:
    std::string color;
    int rating;
public:
    lacksDMA(const std::string & l = "null", const std::string & c = "blank", 
              int r = 0);
    lacksDMA(const DMA & dma, const std::string & c = "blank", 
              int r = 0);
    void View() const;
    friend std::ostream & operator<<(std::ostream & os, 
                                     const lacksDMA & rs);
};

// derived class with DMA
class hasDMA :public DMA
{
private:
    std::string style;
    int rating;
public:
    hasDMA(const std::string & l = "null", const std::string & s = "none", 
              int r = 0);
    hasDMA(const DMA & dma, const std::string & s = "none", 
              int r = 0);
    hasDMA(const hasDMA & hs);
    ~hasDMA();
    void View() const;
    hasDMA & operator=(const hasDMA & rs);  
    friend std::ostream & operator<<(std::ostream & os, 
                                     const hasDMA & rs);
};

#endif
