#include <iostream>
#include <cstring>
#include "pe13-1_cd.h"

// class Cd
Cd::Cd(char * s1, char * s2, int n, double x){
    std::strcpy(performers, s1);
    std::strcpy(label, s2);
    selections = n;
    playtime = x;
}

Cd::Cd(const Cd & d){
    std::strcpy(performers, d.performers);
    std::strcpy(label, d.label);
    selections = d.selections;
    playtime = d.playtime;
}

Cd::Cd(){
    performers[0] = '\0';
    label[0] = '\0';
    selections = 0;
    playtime = 0;
}

Cd::~Cd(){

}

void Cd::Report() const{
    int i = 0;
    std::cout << "Performers: ";
    while(performers[i] != '\0'){
        std::cout << performers[i++];
    }
    i = 0;
    std::cout << std::endl << "Label: ";
    while(label[i] != '\0'){
        std::cout << label[i++];
    }
    std::cout << std::endl << "Selections: " << selections
              << std::endl << "Playtime: " << playtime << "\n";
}

Cd & Cd::operator=(const Cd & d){
    if(this == &d){
        return *this;
    }
    std::strcpy(this->performers, d.performers);
    std::strcpy(this->label, d.label);
    this->selections = d.selections;
    this->playtime = d.playtime;
    return *this;
}


// class Classic
Classic::Classic(char * ms, char * s1, char * s2, int n, double x) 
                : Cd(s1, s2, n, x){
    std::strcpy(mainsong, ms);
}

Classic::Classic(char * ms, const Cd & d)
                : Cd(d){
    std::strcpy(mainsong, ms);
}

Classic::Classic(const Classic & cl)
                : Cd(cl){
    std::strcpy(mainsong, cl.mainsong);
}

Classic::Classic()
                : Cd(){
    mainsong[0] = '\0';
}

Classic::~Classic(){

}

void Classic::Report() const{
    Cd::Report();
    int i = 0;
    std::cout << "Mainsong: ";
    while(mainsong[i] != '\0'){
        std::cout << mainsong[i++];
    }
    std::cout << std::endl;
}

Classic & Classic::operator=(const Classic & cl){
    if(this == &cl){
        return *this;
    }
    Cd::operator=(cl);
    std::strcpy(mainsong, cl.mainsong);
    return *this;
}