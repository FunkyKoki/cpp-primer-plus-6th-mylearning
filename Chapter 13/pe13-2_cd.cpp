#include <iostream>
#include <cstring>
#include "pe13-2_cd.h"

// class Cd
Cd::Cd(char * s1, char * s2, int n, double x){
    performers = new char[std::strlen(s1) + 1];
    label = new char[std::strlen(s2) + 1];
    std::strcpy(performers, s1);
    std::strcpy(label, s2);
    selections = n;
    playtime = x;
}

Cd::Cd(const Cd & d){
    performers = new char[std::strlen(d.performers) + 1];
    label = new char[std::strlen(d.label) + 1];
    std::strcpy(performers, d.performers);
    std::strcpy(label, d.label);
    selections = d.selections;
    playtime = d.playtime;
}

Cd::Cd(){
    performers = NULL;
    label = NULL;
    selections = 0;
    playtime = 0;
}

Cd::~Cd(){
    delete [] performers;
    delete [] label;
}

void Cd::Report() const{
    std::cout << "Performers: " << performers << std::endl;
    std::cout << "Label: " << label;
    std::cout << std::endl << "Selections: " << selections
              << std::endl << "Playtime: " << playtime << "\n";
}

Cd & Cd::operator=(const Cd & d){
    if(this == &d){
        return *this;
    }
    delete [] performers;
    delete [] label;
    performers = new char[std::strlen(d.performers) + 1];
    label = new char[std::strlen(d.label) + 1];
    std::strcpy(performers, d.performers);
    std::strcpy(label, d.label);
    this->selections = d.selections;
    this->playtime = d.playtime;
    return *this;
}


// class Classic
Classic::Classic(char * ms, char * s1, char * s2, int n, double x) 
                : Cd(s1, s2, n, x){
    mainsong = new char[std::strlen(ms) + 1];
    std::strcpy(mainsong, ms);
}

Classic::Classic(char * ms, const Cd & d)
                : Cd(d){
    mainsong = new char[std::strlen(ms) + 1];
    std::strcpy(mainsong, ms);
}

Classic::Classic(const Classic & cl)
                : Cd(cl){
    mainsong = new char[std::strlen(cl.mainsong) + 1];
    std::strcpy(mainsong, cl.mainsong);
}

Classic::Classic()
                : Cd(){
    mainsong = NULL;
}

Classic::~Classic(){
    delete [] mainsong;
}

void Classic::Report() const{
    Cd::Report();
    std::cout << "Mainsong: " << mainsong;
    std::cout << std::endl;
}

Classic & Classic::operator=(const Classic & cl){
    if(this == &cl){
        return *this;
    }
    Cd::operator=(cl);
    delete [] mainsong;
    mainsong = new char[std::strlen(cl.mainsong) + 1];
    std::strcpy(mainsong, cl.mainsong);
    return *this;
}