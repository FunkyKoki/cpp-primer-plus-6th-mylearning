#include "pe13-4_port.h"
#include <cstring>

// Port
Port::Port(const char * br, const char * st, int b){
    brand = new char[std::strlen(br) + 1];
    std::strcpy(brand, br);
    std::strncpy(style, st, std::strlen(st));
    style[std::strlen(st)] = '\0';
    bottles = b;
}

Port::Port(const Port & p){
    brand = new char[std::strlen(p.brand) + 1];
    std::strcpy(brand, p.brand);
    std::strncpy(style, p.style, std::strlen(p.style));
    style[std::strlen(p.style)] = '\0';
    bottles = p.bottles;
}

Port & Port::operator=(const Port & p){
    if(this == & p)
        return *this;
    delete [] brand;
    brand = new char[std::strlen(p.brand) + 1];
    std::strcpy(brand, p.brand);
    std::strncpy(style, p.style, std::strlen(p.style));
    style[std::strlen(p.style)] = '\0';
    bottles = p.bottles;
    return *this;
}

Port & Port::operator+=(int b){
    this->bottles += b;
    return *this;
}

Port & Port::operator-=(int b){
    this->bottles -= b;
    return *this;
}

void Port::Show() const{
    std::cout << "Brand: " << brand << std::endl << "Kind: ";
    int i = 0;
    while(style[i] != '\0'){
        std::cout << style[i++];
    }
    std::cout << std::endl << "Bottles: " << bottles << std::endl;
}

ostream & operator<<(ostream & os, const Port & p){
    os << p.brand << ", ";
    int i = 0;
    while(p.style[i] != '\0'){
        std::cout << p.style[i++];
    }
    std::cout << ", " << p.bottles;
}

// VintagePort
VintagePort::VintagePort():Port(){
    nickname = NULL;
    year = 0;
}

VintagePort::VintagePort(const char * br, int b, const char * nn, int y)
                        : Port(br, "Vintage", b){
    nickname = new char[std::strlen(nn) + 1];
    std::strcpy(nickname, nn);
    year = y;
}

VintagePort::VintagePort(const VintagePort & vp)
                        : Port(vp){
    nickname = new char[std::strlen(vp.nickname) + 1];
    std::strcpy(nickname, vp.nickname);
    year = vp.year;
}

VintagePort & VintagePort::operator=(const VintagePort & vp){
    if(this == &vp)
        return *this;
    Port::operator=(vp);
    delete [] nickname;
    nickname = new char[std::strlen(vp.nickname) + 1];
    std::strcpy(nickname, vp.nickname);
    year = vp.year;
}

void VintagePort::Show() const{
    Port::Show();
    std::cout << "Nickname: " << nickname << std::endl;
    std::cout << "Year: " << year << std::endl;
}

ostream & operator<<(ostream & os, const VintagePort & vp){
    os << (const Port &)vp;
    std::cout << vp.nickname << ", " << vp.year;
}