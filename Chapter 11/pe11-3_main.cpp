#include <iostream>
#include <cstdlib>      // rand(), srand() prototypes
#include <ctime>        // time() prototype
#include "pe11-3_vect.h"
int main()
{
    using namespace std;
    using VECTOR::Vector;
    srand(time(0));     // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    int N = 0;
    int N_ = 0;
    unsigned long maxSteps = 0;
    unsigned long minSteps = 0;
    unsigned long sumSteps = 0;
    cout << "Enter target distance (q to quit): ";
    while (cin >> target)
    {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;
        
        minSteps = 10 * target * target / dstep / dstep;

        cout << "Enter test times: ";
        if (!(cin >> N_))
            break;
        N = N_;

        while(N_ > 0){
            while (result.magval() < target){
                direction = rand() % 360;
                step.reset(dstep, direction, Vector::POL);
                result = result + step;
                steps++;
            }
            sumSteps += steps;
            if(steps > maxSteps){
                maxSteps = steps;
            }
            if(steps < minSteps){
                minSteps = steps;
            }
            steps = 0;
            result.reset(0.0, 0.0);
            N_ = N_ - 1;
        }
        cout << "Max steps: " << maxSteps << endl;
        cout << "Min steps: " << minSteps << endl;
        cout << "Sum steps: " << sumSteps << endl;
        cout << "Mean steps: " << sumSteps/N << endl;
        maxSteps = 0;
        sumSteps = 0;
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    return 0;
}
