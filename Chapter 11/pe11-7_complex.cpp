#include <iostream>
#include "pe11-7_complex.h"

Complex::Complex(){
    real = 0;
    imagine = 0;
}

Complex::Complex(double n1, double n2){
    real = n1;
    imagine = n2;
}

Complex::~Complex(){

}

Complex & Complex::operator+(const Complex & c) const{
    static Complex temp;
    temp.real = real + c.real;
    temp.imagine = imagine + c.imagine;
    return temp;
}

Complex & Complex::operator-(const Complex & c) const{
    static Complex temp;
    temp.real = real - c.real;
    temp.imagine = imagine - c.imagine;
    return temp;
}

Complex & Complex::operator*(const Complex & c) const{
    static Complex temp;
    temp.real = real * c.real - imagine * c.imagine;
    temp.imagine = real * c.imagine + imagine * c.real;
    return temp;
}

Complex & Complex::operator~() const{
    static Complex temp;
    temp.real = real;
    temp.imagine = -imagine;
    return temp;
}

Complex & operator*(double n, const Complex & c){
    static Complex temp;
    temp.real = n * c.real;
    temp.imagine = n * c.imagine;
    return temp;
}

bool operator>>(std::istream & is, Complex & c){
    std::cout << "Input real: ";
    if(std::cin >> c.real){
        std::cout << "Input imagine: ";
        if(std::cin >> c.imagine){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}

std::ostream & operator<<(std::ostream & os, const Complex & c){
    std::cout << c.real;
    if(c.imagine >= 0){
        std::cout << "+" << c.imagine << "i" << std::endl;
    }
    else{
        std::cout << c.imagine << "i" << std::endl;
    }
    return os;
}