#include <iostream>
#include "pe11-5_stonewt.h"

int main(int argc, char const *argv[])
{
    {
        Stonewt p(12.4);
        std::cout << p << std::endl;
        p.toPdsInt();
        std::cout << p << std::endl;
        p.toStone();
        std::cout << p << std::endl;
        p.toPdsFlo();
        std::cout << p << std::endl;
    }
    return 0;
}
