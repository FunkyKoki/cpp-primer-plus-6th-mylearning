// randwalk.cpp -- using the Vector class
// compile with the vect.cpp file
#include <iostream>
#include <fstream>
#include <cstdlib>      // rand(), srand() prototypes
#include <ctime>        // time() prototype
#include "pe11-1_vect.h"
int main()
{
    using namespace std;
    ofstream fout;
    fout.open("thewalk.txt");
    using VECTOR::Vector;
    srand(time(0));     // seed random-number generator
    static int count = 0;
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    cout << "Enter target distance (q to quit): ";
    while (cin >> target)
    {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;

        fout << "Target Distance: " << target 
             << ", Step Size: " << dstep << endl;
        while (result.magval() < target)
        {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            steps++;
            fout << (count++) << ": " << result << endl;
        }
        fout << "After " << (count - 1) << " steps, the subject has the following location:\n"
             << result << endl;
        result.polar_mode();
        fout << "or\n" << result <<endl;
        fout << "Average outward distance per step = " << target / count;
        count = 0;
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
/* keep window open
    cin.clear();
    while (cin.get() != '\n')
        continue;
    cin.get();
*/
    return 0;
}
