#ifndef COMPLEX0_H
#define COMPLEX0_H

#include <iostream>

class Complex{
private:
    double real;
    double imagine;
public:
    Complex();
    Complex(double, double);
    ~Complex();
    Complex & operator+(const Complex &) const;
    Complex & operator-(const Complex &) const;
    Complex & operator*(const Complex &) const;
    Complex & operator~() const;
    friend Complex & operator*(double, const Complex &);
    friend bool operator>>(std::istream &, Complex &);
    friend std::ostream & operator<<(std::ostream &, const Complex &);
};

#endif