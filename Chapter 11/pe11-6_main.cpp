#include <iostream>
#include "pe11-6_stonewt.h"

int main(int argc, char const *argv[])
{
    {
        Stonewt compare(11, 0);
        Stonewt p[6] = {
            Stonewt(15.1),
            Stonewt(1, 1.2),
            Stonewt(56.0)
        };
        for(int i = 0; i < 3; i++){
            double temp;
            std::cout << "Input pounds(float): ";
            std::cin >> temp;
            p[i+3] = Stonewt(temp);
        }
        int max = 0, min = 0, equal = 0;
        for(int i = 1; i < 6; i++){
            if(p[i] > p[max]){
                max = i;
            }
            if(p[i] < p[min]){
                min = i;
            }
            if(p[i] == compare){
                equal++;
            }
        }
        std::cout << "Max: ";
        p[max].show_lbs();
        std::cout << "Min: ";
        p[min].show_lbs();
        std::cout << "Equal number: " << equal << std::endl;
    }
    return 0;
}
