// stonewt.cpp -- Stonewt methods
#include <iostream>
using std::cout;
#include "pe11-5_stonewt.h"

// construct Stonewt object from double value
Stonewt::Stonewt(double n, Mode m)
{
    if(mode == STN){
        stone = int(n);
        pds_int = int(n) * Lbs_per_stn;
        pds_float = n * Lbs_per_stn;
        pds_left = 0;
    }
    else if(mode == PDSint){
        stone = int(n) / Lbs_per_stn;
        pds_int = int(n);
        pds_float = n;
        pds_left = int (n) % Lbs_per_stn;
    }
    else if(mode == PDSflo){
        stone = int(n) / Lbs_per_stn;
        pds_int = int(n);
        pds_float = n;
        pds_left = int (n) % Lbs_per_stn + n - int(n);
    }
}

// construct Stonewt object from stone, double values
// Stonewt::Stonewt(int stn, double lbs)
// {
//     stone = stn;
//     pds_left = lbs;
//     pounds =  stn * Lbs_per_stn +lbs;
// }

Stonewt::Stonewt()          // default constructor, wt = 0
{
    stone = pds_int = pds_float = pds_left = 0;
}

Stonewt::~Stonewt()         // destructor
{
}

void Stonewt::toStone(){
    mode = STN;
}

void Stonewt::toPdsInt(){
    mode = PDSint;
}

void Stonewt::toPdsFlo(){
    mode = PDSflo;
}

std::ostream & operator<<(std::ostream & os, const Stonewt & s){
    if(s.mode == s.STN){
        os << "Stone: " << s.stone << std::endl;
    }
    else if(s.mode == s.PDSint){
        os << "Pounds(int): " << s.pds_int << std::endl;
    }
    else if(s.mode == s.PDSflo){
        os << "Pounds(float): " << s.pds_float << std::endl;
    }
}
