// stonewt.h -- definition for the Stonewt class
#ifndef STONEWT_H_
#define STONEWT_H_
class Stonewt
{
private:
    enum {Lbs_per_stn = 14};      // pounds per stone
    enum Mode {STN, PDSint, PDSflo};
    int stone;                    // whole stones
    int pds_int;
    double pds_float;             // entire weight in pounds
    double pds_left;              // fractional pounds
    Mode mode = PDSflo;
public:
    Stonewt(double lbs, Mode m = PDSflo);          // constructor for double pounds
    // Stonewt(int stn, double lbs, Mode m = PDSflo); // constructor for stone, lbs
    Stonewt();                    // default constructor
    ~Stonewt();
    void toStone();
    void toPdsInt();
    void toPdsFlo();
    friend std::ostream & operator<<(std::ostream & os, const Stonewt & s);
};
#endif
