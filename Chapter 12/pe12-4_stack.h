#ifndef STACK_H_
#define STACK_H_

#include <iostream>

typedef unsigned long Item;

class Stack{
private:
    enum {MAX = 10};
    Item * pitems;
    int size;
    int top;
public:
    Stack(int n = MAX);
    Stack(const Stack &);
    ~Stack();
    bool isempty() const;
    bool isfull() const;
    bool push(const Item &);
    bool pop(Item &);
    Stack & operator=(const Stack &);
    friend std::ostream & operator<<(std::ostream &, const Stack &);
};

#endif