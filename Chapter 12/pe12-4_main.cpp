#include <iostream>
#include "pe12-4_stack.h"

int main(int argc, char const *argv[])
{
    {
        Stack s(5);
        Item a[5] = {20, 10, 2000, 300, 0};
        for(int i = 0; i < 5; i++){
            s.push(a[i]);
        }
        Stack s2 = s;
        Stack s3;
        s3 = s2;
        Item temp;
        s.pop(temp);
        s.pop(temp);
        std::cout << s << s2 << s3;
    }
    return 0;
}
