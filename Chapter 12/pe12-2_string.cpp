#include "pe12-2_string.h"

String::String(){
    str = NULL;
}

String::String(const char * s){
    str = new char[std::strlen(s) + 1];
    std::strcpy(str, s);
}

String::String(const String & s){
    str = new char[std::strlen(s.str) + 1];
    std::strcpy(str, s.str);
}

String::~String(){
    delete [] str;
}

String & String::stringup(){
    int len = std::strlen(str);
    for(int i = 0; i < len; i++){
        if(str[i] >= 'a' && str[i] <= 'z'){
            str[i] -= 32;
        }
    }
    return *this;
}

String & String::stringlow(){
    int len = std::strlen(str);
    for(int i = 0; i < len; i++){
        if(str[i] >= 'A' && str[i] <= 'Z'){
            str[i] += 32;
        }
    }
    return *this;
}

int String::has(char c) const{
    int len = std::strlen(str), sum = 0;
    for(int i = 0; i < len; i++){
        if(str[i] == c){
            sum++;
        }
    }
    return sum;
}

bool String::operator==(const char * s) const{
    if(std::strlen(str) != std::strlen(s)){
        return false;
    }
    else{
        int len = std::strlen(str);
        for(int i = 0; i < len; i++){
            if(str[i] != s[i]){
                return false;
            }
        }
        return true;
    }
}

bool String::operator==(const String & s) const{
    if(std::strlen(str) != std::strlen(s.str)){
        return false;
    }
    else{
        int len = std::strlen(str);
        for(int i = 0; i < len; i++){
            if(str[i] != s.str[i]){
                return false;
            }
        }
        return true;
    }
}

String & String::operator=(const String & s){
    if(this == &s){
        return *this;
    }
    else{
        delete [] str;
        this->str = new char[std::strlen(s.str) + 1];
        std::strcpy(this->str, s.str);
        return *this;
    }
}

String String::operator+(const String & s){
    String temp;
    char * sum = new char[std::strlen(this->str) + std::strlen(s.str) + 1];
    std::strcpy(sum, this->str);
    std::strcpy(sum + std::strlen(this->str), s.str);
    temp.str = sum;
    return temp;
}

String operator+(char * c, const String & s){
    String temp;
    char * sum = new char[std::strlen(c) + std::strlen(s.str) + 1];
    std::strcpy(sum, c);
    std::strcpy(sum + std::strlen(c), s.str);
    temp.str = sum;
    return temp;
}

std::ostream & operator<<(std::ostream & os, const String & s){
    std::cout << s.str;
    return os;
}

bool operator>>(std::istream & is, String & s){
	delete [] s.str;
	char * fullname = new char[20];
	char next;
	std::cin.get(fullname, 20);
	if(std::cin){
		std::cin.get(next);
        while(next != '\n'){
            std::cin.get(next);
        }
        s.str = fullname;
        return true;
	}
	else{
		return false;
	}
}
