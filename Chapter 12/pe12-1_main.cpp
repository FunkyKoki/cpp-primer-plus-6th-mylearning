#include <iostream>
#include "pe12-1_cow.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	{
		Cow p("JianFan", "Eat and Sleep", 160);
		Cow q("jsp", "sl&eat", 120);
		q = p;
		p.ShowCow();
		q.ShowCow();
	}
	return 0;
}
