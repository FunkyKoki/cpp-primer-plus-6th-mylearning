#ifndef STRING_H_
#define STRING_H_

#include <iostream>
// #include <algorithm>
#include <cstring>

class String{
private:
    char * str;
public:
    String();
    String(const char *);
    String(const String &);
    ~String();
    String & stringup();
    String & stringlow();
    int has(char) const;
    bool operator==(const char *) const;
    bool operator==(const String &) const;
    String & operator=(const String &);
    String operator+(const String &);
    friend String operator+(char *, const String &);
    friend std::ostream & operator<<(std::ostream &, const String &);
    friend bool operator>>(std::istream &, String &);
};

#endif