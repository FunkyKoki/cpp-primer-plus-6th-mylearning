#include <iostream>
#include <cstring>
#include "pe12-4_stack.h"

Stack::Stack(int n){
    if(n > MAX){
        n = MAX;
    }
    pitems = new Item[n];
    size = n;
    top = 0;
}

Stack::Stack(const Stack & s){
    pitems = new Item[s.size];
    for(int i = 0; i < s.size; i++){
        pitems[i] = s.pitems[i];    
    }
    size = s.size;
    top = s.top;
}

Stack::~Stack(){
    delete [] pitems;
}

bool Stack::isempty() const{
    if(top == 0){
        return true;
    }
    else{
        return false;
    }
}

bool Stack::isfull() const{
    if(top == size){
        return true;
    }
    else{
        return false;
    }
}

bool Stack::push(const Item & i){
    if(this->isfull()){
        return false;
    }
    else{
        pitems[top++] = i;
        return true;
    }
}

bool Stack::pop(Item & i){
    if(this->isempty()){
        return false;
    }
    else{
        i = pitems[--top];
        return true;
    }
}

Stack & Stack::operator=(const Stack & s){
    if(this == &s){
        return *this;
    }
    delete [] pitems;
    pitems = new Item[s.size];
    for(int i = 0; i < s.size; i++){
        pitems[i] = s.pitems[i];    
    }
    size = s.size;
    top = s.top;
}

std::ostream & operator<<(std::ostream & os, const Stack & s){
    for(int i = 0; i < s.top; i++){
        std::cout << s.pitems[i] << " ";
    }
    std::cout << std::endl;
}