#include <iostream>
#include <cstring>
#include "pe12-1_cow.h"

Cow::Cow(){
	name[0] = '\0';
	hobby = NULL;
	weight = 0;
}

Cow::Cow(const char * nm, const char * ho, double wt){
	std::strcpy(name, nm);
	hobby = new char[std::strlen(ho) + 1];
	std::strcpy(hobby, ho);
	weight = wt;
}

Cow::Cow(const Cow & c){
	std::strcpy(name, c.name);
	hobby = new char[std::strlen(c.hobby) + 1];
	std::strcpy(hobby, c.hobby);
	weight = c.weight;
}

Cow::~Cow(){
	delete [] hobby;
}

Cow & Cow::operator=(const Cow & c){
	if(this == &c){
		return *this;
	}
	delete [] hobby;
	std::strcpy(this->name, c.name);
	this->hobby = new char[std::strlen(c.hobby) + 1];
	std::strcpy(this->hobby, c.hobby);
	this->weight = c.weight;
	return *this;
}

void Cow::ShowCow() const{
	std::cout << "  Name: " << name << std::endl;
	std::cout << " Hobby: " << hobby << std::endl;
	std::cout << "Weight: " << weight << std::endl;
}
