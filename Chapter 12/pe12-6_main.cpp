// bank.cpp -- using the Queue interface
// compile with queue.cpp
#include <iostream>
#include <cstdlib> // for rand() and srand()
#include <ctime>   // for time()
#include <cmath>
#include "pe12-6_queue.h"

const int MIN_PER_HR = 60;

bool newcustomer(double x); // is there a new customer?

int main()
{
    using std::cin;
    using std::cout;
    using std::endl;
    using std::ios_base;
/*JSP add*/
    double perhour = 10;
    double avgwaittime = 0.1, lastavgwaittime = 2.0;
    while( fabs(avgwaittime - lastavgwaittime) >= 0.01 && 
           (fabs(avgwaittime - 1.0) < fabs(lastavgwaittime - 1.0)) ){
    // setting things up
        std::srand(std::time(0));    //  random initializing of rand()

        Queue line1(10);         // line queue holds up to qs people
        Queue line2(10);

        long cyclelimit = MIN_PER_HR * 10000; // # of cycles

        double min_per_cust;    //  average time between arrivals
        min_per_cust = MIN_PER_HR / perhour;

        Item temp;              //  new customer data
        long turnaways = 0;     //  turned away by full queue
        long customers = 0;     //  joined the queue
        long served = 0;        //  served during the simulation
        int wait_time1 = 0;      //  time until autoteller is free
        int wait_time2 = 0;
        long line_wait = 0;     //  cumulative time in line

        for (int cycle = 0; cycle < cyclelimit; cycle++)
        {
            if (newcustomer(min_per_cust))  // have newcomer
            {
                if (line1.isfull() && line2.isfull())
                    turnaways++;
                else if(line1.isfull()){
                    customers++;
                    temp.set(cycle);    // cycle = time of arrival
                    line2.enqueue(temp);
                }
                else if(line1.queuecount() > line2.queuecount()){
                    customers++;
                    temp.set(cycle);    // cycle = time of arrival
                    line2.enqueue(temp);
                }
                else{
                    customers++;
                    temp.set(cycle);    // cycle = time of arrival
                    line1.enqueue(temp); // add newcomer to line
                }
            }
            if (wait_time1 <= 0 && !line1.isempty())
            {
                line1.dequeue (temp);      // attend next customer
                wait_time1 = temp.ptime(); // for wait_time minutes
                line_wait += cycle - temp.when();
                served++;
            }
            if (wait_time1 > 0)
                wait_time1--;
            if (wait_time2 <= 0 && !line2.isempty())
            {
                line2.dequeue (temp);      // attend next customer
                wait_time2 = temp.ptime(); // for wait_time minutes
                line_wait += cycle - temp.when();
                served++;
            }
            if (wait_time2 > 0)
                wait_time2--;
        }

        if (customers > 0)
        {
            lastavgwaittime = avgwaittime;
            avgwaittime = (double) line_wait / served;
            cout << " avg: " << avgwaittime << "  last: " << lastavgwaittime << endl;
        }
        else
            cout << "No customers!\n";
        perhour++;
    }
    cout << "#Best# perhour: " << (perhour - 2) << endl;
    return 0;
}

bool newcustomer(double x)
{
    return (std::rand() * x / RAND_MAX < 1); 
}
