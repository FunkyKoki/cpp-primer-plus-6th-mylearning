#include "pe14-4_person.h"

const int SIZE = 3;

int main(){
	{
		using std::cout;
		using std::cin;
		using std::endl;

		Person * p[SIZE];

		p[0] = new Gunslinger("Champagne", "Jin", 15);
		p[1] = new Pokerplayer("Peng", "Han");
		p[2] = new BadDude("Tong", "Su", 666);

		for (int i = 0; i < SIZE; i++){
			p[i]->show();
		}
	}
	return 0;
}