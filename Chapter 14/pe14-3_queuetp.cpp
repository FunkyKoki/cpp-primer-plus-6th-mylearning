#include "pe14-3_queuetp.h"

// Worker methods
Worker::~Worker() { }
//protected methods
void Worker::Data() const
{
    std::cout << "Name: " << fullname << std::endl;
    std::cout << "Employee ID: " << id << std::endl;
}

void Worker::Get()
{
    std::getline(std::cin, fullname);
    std::cout << "Enter worker's ID: ";
    std::cin >> id;
    while (std::cin.get() != '\n')
        continue;
}

void Worker::show() const{
	std::cout << fullname << "  " << id << std::endl;
}
