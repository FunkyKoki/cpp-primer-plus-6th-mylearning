#ifndef WINE2_H_
#define WINE2_H_

#include <iostream>
#include <string>
#include <valarray>
#include <utility>

typedef std::valarray<int> ArrayInt;
typedef std::pair<ArrayInt, ArrayInt> PairArray;

class Wine : private std::string, private PairArray{
private:
    int yrs;
public:
    Wine() {}
    Wine(const char * l, int y, const int yr[], const int bot[]);
    Wine(const char * l, int y);
    void GetBottles();
    std::string Label() const;
    int sum() const;
    void Show() const;
};

#endif