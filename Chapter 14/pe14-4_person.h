#ifndef PERSON_H_
#define PERSON_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

class Person{
private:
	std::string firstname;
	std::string lastname;
public:
	Person(std::string fn = "defaultFn", std::string ln = "defaultLn");
	virtual void show() const;
};

class Gunslinger : virtual public Person{
private:
	int gunSkrews;
public:
	Gunslinger(std::string fn = "defaultFn", std::string ln = "defaultLn", int s = 0);
	Gunslinger(const Person & p, int s = 0);
	double Draw() const;
	virtual void show() const;
};

class Pokerplayer : virtual public Person{
public:
	Pokerplayer(std::string fn = "defaultFn", std::string ln = "defaultLn");
	Pokerplayer(const Person & p);
	int Draw() const;
};

class BadDude : public Gunslinger, public Pokerplayer{
public:
	BadDude(std::string fn = "defaultFn", std::string ln = "defaultLn", int s = 0);
	BadDude(const Person & p, int s = 0);
	double Gdraw() const;
	int Cdraw() const;
	void show() const;
};

#endif