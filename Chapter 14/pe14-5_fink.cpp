#include "pe14-5_fink.h"

abstr_emp::abstr_emp(){
    fname = "defaultFn";
    lname = "defaultLn";
    job = "NULL";
}

abstr_emp::abstr_emp(const std::string & fn, const std::string & ln,
    const std::string & j)
    : fname(fn), lname(ln), job(j){

}

void abstr_emp::ShowAll() const{
    std::cout << "Name: " << fname << " " << lname << std::endl;
    std::cout << "Job: " << job << std::endl;
}

void abstr_emp::SetAll(){
    std::string temp;
    std::cout << "Input firstname: ";
    std::cin >> temp;
    fname = temp;
    std::cout << "Input lastname: ";
    std::cin >> temp;
    lname = temp;
    std::cout << "Input your job: ";
    std::cin >> temp;
    job = temp;
}

std::ostream & operator<<(std::ostream & os, const abstr_emp & e){
    os << e.fname << " " << e.lname << " " << e.job;
	return os;
}

abstr_emp::~abstr_emp(){

}

employee::employee()
    : abstr_emp(){

}

employee::employee(const std::string & fn, const std::string & ln,
    const std::string & j)
    : abstr_emp(fn, ln, j){

}

void employee::ShowAll() const{
    abstr_emp::ShowAll();
}

void employee::SetAll(){
    abstr_emp::SetAll();
}

manager::manager() : abstr_emp(), inchargeof(0){

}

manager::manager(const std::string & fn, const std::string & ln, 
	const std::string & j, int ico)
    : abstr_emp(fn, ln, j), inchargeof(ico){

}

manager::manager(const abstr_emp & e, int ico)
    : abstr_emp(e), inchargeof(ico){

}

manager::manager(const manager & m) 
	: abstr_emp(m), inchargeof(m.inchargeof){

}

void manager::ShowAll() const{
	abstr_emp::ShowAll();
	std::cout << "Person in charge of: " << inchargeof << std::endl;
}

void manager::SetAll(){
	abstr_emp::SetAll();
	int temp;
	std::cout << "Input person number you in charge of: ";
	std::cin >> temp;
	inchargeof = temp;
}

fink::fink()
	: abstr_emp(), reportsto("NULL"){

}

fink::fink(const std::string & fn, const std::string & ln,
	const std::string & j, const std::string & rpo)
	: abstr_emp(fn, ln, j), reportsto(rpo){

}

fink::fink(const abstr_emp & e, const std::string & rpo)
	: abstr_emp(e), reportsto(rpo){

}

fink::fink(const fink & e)
	: abstr_emp(e), reportsto(e.reportsto){

}

void fink::ShowAll() const{
	abstr_emp::ShowAll();
	std::cout << "Report to: " << reportsto << std::endl;
}

void fink::SetAll(){
	abstr_emp::SetAll();
	std::string temp;
	std::cout << "Input person you report to: ";
	std::cin >> temp;
	reportsto = temp;
}

highfink::highfink()
	: abstr_emp(), manager(), fink(){

}

highfink::highfink(const std::string & fn, const std::string & ln,
	const std::string & j, const std::string & rpo,
	int ico)
	: abstr_emp(fn, ln, j), manager(fn, ln ,j, ico), fink(fn, ln, j, rpo){

}

highfink::highfink(const abstr_emp & e, const std::string & rpo,
	int ico)
	: abstr_emp(e), manager(e, ico), fink(e, rpo){

}

highfink::highfink(const fink & f, int ico)
	: abstr_emp(f), manager(f, ico), fink(f){

}

highfink::highfink(const manager & m, const std::string & rpo)
	: abstr_emp(m), manager(m), fink(m, rpo){

}

highfink::highfink(const highfink & h)
	: abstr_emp(h), manager(h), fink(h){

}

void highfink::ShowAll() const{
	abstr_emp::ShowAll();
	std::cout << "Person in charge of: " << InChargeOf() << std::endl;
	std::cout << "Report to: " << ReportsTo() << std::endl;
}

void highfink::SetAll(){
	abstr_emp::SetAll();
	int & i = InChargeOf();
	int temp1;
	std::cout << "Input person number you in charge of: ";
	std::cin >> temp1;
	i = temp1;
	std::string & r = ReportsTo();
	std::string temp2;
	std::cout << "Input person you report to: ";
	std::cin >> temp2;
	r = temp2;
}