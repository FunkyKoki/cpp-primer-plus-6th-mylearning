#include "pe14-4_person.h"

Person::Person(std::string fn, std::string ln){
	firstname = fn;
	lastname = ln;
}

void Person::show() const{
	std::cout << "Name: " << firstname << " "
		<< lastname << std::endl;
}

Gunslinger::Gunslinger(std::string fn, std::string ln, int s)
	: Person(fn, ln), gunSkrews(s){

}

Gunslinger::Gunslinger(const Person & p, int s)
	: Person(p), gunSkrews(s){

}

double Gunslinger::Draw() const{
	std::srand(std::time(0));
	return double((rand() % 101) / 100);
}

void Gunslinger::show() const{
	Person::show();
	std::cout << "Gunskrews: " << gunSkrews << std::endl;
}

Pokerplayer::Pokerplayer(std::string fn, std::string ln)
	: Person(fn, ln){

}

Pokerplayer::Pokerplayer(const Person & p)
	: Person(p){

}

int Pokerplayer::Draw() const{
	std::srand(std::time(0));
	return rand() % 52 + 1;
}

BadDude::BadDude(std::string fn, std::string ln, int s)
	: Person(fn, ln), Gunslinger(fn, ln, s), Pokerplayer(fn, ln){

}

BadDude::BadDude(const Person & p, int s)
	: Person(p), Gunslinger(p, s), Pokerplayer(p){

}

double BadDude::Gdraw() const{
	return ((const Gunslinger &)(*this)).Draw();
}

int BadDude::Cdraw() const{
	return ((const Pokerplayer &)(*this)).Draw();
}

void BadDude::show() const{
	Person::show();
	std::cout << "Gunskrews: " << Gdraw() << "\t"
		<< "Pokercard: " << Cdraw() << std::endl;
}