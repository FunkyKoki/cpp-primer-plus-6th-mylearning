#ifndef WINE_H_
#define WINE_H_

#include <iostream>
#include <string>
#include <valarray>
#include <utility>

typedef std::valarray<int> ArrayInt;
typedef std::pair<ArrayInt, ArrayInt> PairArray;

class Wine{
private:
    std::string label;
    int yrs;
    PairArray yrandBot;
public:
	Wine(){}
    Wine(const char * l, int y, const int yr[], const int bot[]);
    Wine(const char * l, int y);
    void GetBottles();
    std::string Label() const;
    int sum() const;
    void Show() const;
};

#endif