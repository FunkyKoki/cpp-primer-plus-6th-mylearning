#include "pe14-2_wine.h"

Wine::Wine(const char * l, int y, const int yr[], const int bot[])
    : std::string(l), yrs(y),
      PairArray(ArrayInt(yr, y), ArrayInt(bot, y)){
    
}

Wine::Wine(const char * l, int y)
	: std::string(l), yrs(y),
	  PairArray(ArrayInt(y), ArrayInt(y)){
    
}

void Wine::GetBottles(){
    std::cout << "Enter " << (std::string &)(*this) 
              << " data for " << yrs << " year(s):\n";
    for(int i = 0; i < yrs; i++){
        std::cout << "Enter year: ";
        std::cin >> ((PairArray &)(*this)).first[i];
        std::cout << "Enter bottles for that year: ";
        std::cin >> ((PairArray &)(*this)).second[i];
    }
}

std::string Wine::Label() const{
	return (const std::string &)(*this);
}

int Wine::sum() const{
    int summary = ((const PairArray &)(*this)).second[0];
    for(int i = 1; i < yrs; i++){
        summary += ((const PairArray &)(*this)).second[i];
    }
    return summary;
}

void Wine::Show() const{
	std::cout << "Wine: " << (const std::string &)(*this) << "\n";
    std::cout << "\tYear\t" << "Bottles\n";
    for(int i = 0; i < yrs; i++){
        std::cout << "\t" << ((const PairArray &)(*this)).first[i] << "\t" 
                  << ((const PairArray &)(*this)).second[i] << "\n";
    }
}
