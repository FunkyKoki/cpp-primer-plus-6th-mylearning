#ifndef QUEUETP_H_
#define QUEUETP_H_
#include <iostream>
#include <string>

class Worker   // an abstract base class
{
private:
    std::string fullname;
    long id;
protected:
    virtual void Data() const;
    virtual void Get();
public:
    Worker() : fullname("no one"), id(0L) {}
    Worker(const std::string & s, long n)
            : fullname(s), id(n) {}
    virtual ~Worker(); // pure virtual function
	void show() const;
};

template<typename Type>
class Queue{
private:
	struct Node{ Type item; struct Node * next; };
	enum {SIZE = 10};
	Node * front;
	Node * rear;
    const int maxsize;
    int currsize;
	Queue(const Queue<Type> & q) : maxsize(0) {}
	Queue<Type> & operator=(const Queue<Type> & q) { return *this; }
public:
    explicit Queue(int s = SIZE);
	~Queue();
    bool isempty() const;
    bool isfull() const;
    bool push(const Type & i);
    bool pop(Type & i);
};

template<typename Type>
Queue<Type>::Queue(int s)
	       : maxsize(s), currsize(0), front(NULL), rear(NULL){
	
}

template<typename Type>
Queue<Type>::~Queue(){
	Node * temp;
	while (front != NULL)   // while queue is not yet empty
	{
		temp = front;       // save address of front item
		front = front->next;// reset pointer to next item
		delete temp;        // delete former front
	}
}

template<typename Type>
bool Queue<Type>::isempty() const{
	return currsize == 0;
}

template<typename Type>
bool Queue<Type>::isfull() const{
	return currsize == maxsize;
}

template<typename Type>
bool Queue<Type>::push(const Type & i){
	if (isfull())
		return false;
	Node * add = new Node;
	add->item = i;
	add->next = NULL;
	currsize++;
	if (front == NULL)
		front = add;
	else
		rear->next = add;
	rear = add;
	return true;
}

template<typename Type>
bool Queue<Type>::pop(Type & i){
	if (front == NULL)
		return false;
	i = front->item;
	currsize--;
	Node * temp = front;
	front = front->next;
	delete temp;
	if (currsize == 0)
		rear = NULL;
	return true;
}

#endif