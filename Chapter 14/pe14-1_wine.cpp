#include "pe14-1_wine.h"

Wine::Wine(const char * l, int y, const int yr[], const int bot[])
	: yrandBot(ArrayInt(yr, y), ArrayInt(bot, y)){
    label = l;
    yrs = y;
}

Wine::Wine(const char * l, int y)
	: yrandBot(ArrayInt(y), ArrayInt(y)){
    label = l;
    yrs = y;
}

void Wine::GetBottles(){
    std::cout << "Enter " << label << " data for " << yrs << " year(s):\n";
    for(int i = 0; i < yrs; i++){
        std::cout << "Enter year: ";
        std::cin >> yrandBot.first[i];
        std::cout << "Enter bottles for that year: ";
        std::cin >> yrandBot.second[i];
    }
}

std::string Wine::Label() const{
	return (*this).label;
}

int Wine::sum() const{
    int summary = yrandBot.second[0];
    for(int i = 1; i < yrs; i++){
        summary += yrandBot.second[i];
    }
    return summary;
}

void Wine::Show() const{
	std::cout << "Wine: " << label << "\n";
    std::cout << "\tYear\t" << "Bottles\n";
    for(int i = 0; i < yrs; i++){
        std::cout << "\t" << yrandBot.first[i] << "\t" << yrandBot.second[i] << "\n";
    }
}
