#include "pe14-3_queuetp.h"
#include <iostream>

const int SIZE = 3;

int main(int argc, char const *argv[])
{
    {
        using std::cin;
        using std::cout;
        using std::endl;

        Queue<Worker *> p(SIZE);
		Worker * s[SIZE] = {
			new Worker("JSP", 2026),
		    new Worker("HP", 2020),
		    new Worker("ST", 2048)};

        for(int i = 0; i < SIZE; i++){
            p.push(s[i]);
        }

		Worker * t;
		for (int i = 0; i < SIZE; i++){
			p.pop(t);
			t->show();
		}

		cout << p.isempty() << endl;
    }
    return 0;
}
